#define F_CPU 8000000    // AVR clock frequency in Hz, used by util/delay.h
#include <avr/io.h>
#include <util/delay.h>

#define  bit_mask(bit)          (1<<(bit))
#define  set_bit(reg,bit)       ((reg) |= bit_mask(bit))
#define  clear_bit(reg,bit)     ((reg) &= ~bit_mask(bit))
#define  toggle_bit(reg,bit)    ((reg) ^= bit_mask(bit))
#define  check_bit(reg,bit)     ((reg) & (bit_mask(bit)))

int main() {
    set_bit(DDRC,0);
    set_bit(DDRC,4);
    set_bit(PORTC,4);
    /*
     *DDRC |= (1<<0);        // set LED pin PC0 to output
     *DDRC |= (1<<4);        // set LED pin PC4 to output
     *PORTC |= (1<<4);       // drive PC4 high
     */
    while (1) {
        set_bit(PORTC,0);
        /*PORTC |= (1<<0);   // drive PD1 high*/
        _delay_ms(500);    // delay 100 ms
        /*PORTC &= ~(1<<0);  // drive PD1 low*/
        clear_bit(PORTC,0);
        _delay_ms(500);    // delay 900 ms
    }
}
